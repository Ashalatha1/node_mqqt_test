const express = require('express');
const app = express();
const port = 3000;
var timer_id = null;
var topic = "dccpl/test/ltts/nodejs-demo-app";
var mqtt = require('mqtt');
const url = "mqtt://broker.hivemq.com";
var options = {
clientId: "mqttnode"
}

var client = mqtt.connect(url, options);
client.on("connect", function () { // when connected
 client.subscribe(topic, function () {
      client.on('message', function (topic, message) {
     console.log("Received '" + message + "' on '" + topic + "'");
      });
});
 });

function publish(topic) {
    let message = {
        CurrentTimeStamp: Date.now().toLocaleString(),
        currentValue: Date.now() + 1,
    }
    if (client.connected == true) {
        client.publish(topic,message);
		
    }
	
	client.end();
}

app.post('/', (req, res) => {
    var mode = req.query.mode
	var timestamp_id;
    if (mode == 'START') {
        timestamp_id = setInterval(function () { publish(topic); }, 3000);
        res.send('Started! Message Will send every 3 sec');
    }
    else if (mode == 'STOP') {
        if (timestamp_id != null) {
            clearTimeout(timestamp_id);
            timestamp_id = null;
            res.send('Stoped');
        }
    }
});
app.get('/', (req, res) => {
    if (timestamp_id == null) {
        res.send({ Status: "STOPPED", Topic: topic });
    }
    else
    {
        res.send({ Status: "RUNNING", Topic: topic });
    }
});



app.listen(port, () => console.log(`Hello world app listening on port ${port}!`))
